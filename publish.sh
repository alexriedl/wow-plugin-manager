#!/usr/bin/env bash

OUTPUTTED_EXECUTABLE_NAME="output/wpm"
OUTPUTTED_INSTALLER_NAME="output/installer.sh"

rm -rf output

echo "Publishing"

dotnet publish                       \
  --runtime "linux-x64"              \
  --output output                    \
  --configuration Release            \
  /p:PublishSingleFile=true          \
  /p:PublishTrimmed=true             \
  /p:IncludeSymbolsInSingleFile=true \
  src/console/console.csproj
[ $? -eq 0 ] || exit 1

echo "Copying Installer"
cp installer.sh "${OUTPUTTED_INSTALLER_NAME}"
[ $? -eq 0 ] || exit 2

echo "Configuring Installer"
EXECUTABLE_BYTE_SIZE=$(stat --printf="%s" "${OUTPUTTED_EXECUTABLE_NAME}")
sed -i "s/REPLACE_ME__BYTE_SIZE/${EXECUTABLE_BYTE_SIZE}/g" "${OUTPUTTED_INSTALLER_NAME}"
[ $? -eq 0 ] || exit 3

echo "Outputing Executable"
tail -c "${EXECUTABLE_BYTE_SIZE}" "${OUTPUTTED_EXECUTABLE_NAME}" >> "${OUTPUTTED_INSTALLER_NAME}"
[ $? -eq 0 ] || exit 4
