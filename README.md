# Overview
This is a cross platform tool used to manage WoW plugins. The intent is to be
something less intrusive that the current Twitch app. Also the primary goal is
to allow and encourage downloading and update addons directly from the source
control repository.

# Getting Started
Precompiled binaries can be downloaded from the pipelines page.

* To install: as admin run `./wpm_installer install`
* Once installed initialize the application `/opt/wpm/cli install initialize` # do not run this as root
* Now you can update your plugins by running `/opt/wpm/cli plugin update`
  * Recommended to make a simple script to run the update from you and put that script in a convenient place to run frequently

Example Script to put on your desktop
```bash
#!/usr/bin/env bash

/opt/wpm/cli plugin update
```


## Running from source
1. Clone this repository
2. Install dotnet core 3.1
3. At the root of the project run `./cli.sh install initialize` to initialize the files you need
4. Add plugins to the `user-config.json` in your .config directory
5. Run `./cli.sh plugin update` to install and update all plugins

# Supported plugin addresses and formats
Below are a few examples of support formats for WoW plugin urls

## Git
* "git@github.com:Hoizame/AtlasLootClassic.git",         // Normal ssh git address
* "https://github.com/Hoizame/AtlasLootClassic.git",     // Normal https git address
* "https://bitbucket.org/yunohu/reciperadarclassic.git", // Any git address should work just fine (not required to be github)

To pin to a specific branch or tag, turn the line into a complex object. View
the example user-config.json file in this readme

## Github Releases
* "https://github.com/Hoizame/AtlasLootClassic",     // URL to github project that has github releases

To pin to a specific tag, turn the line into a complex object. View the example
user-config.json file in this readme. This is done the same way as a normal git
project

## Local Files
Local zips dont update. They will be installed once, but after that it is
assumed they are always up-to-date. To update any local file plugin, simply
keep the version number in the file name. This will make the installer think
the old version should be uninstalled and the new version be installed.
* "file:///home/test/Downloads/SimpleEnergyBar-v1.0.11.zip",

## WowInterface
Get the url by finding the plugin on the wowinterface.com website. Copy download path from the download button.
* "https://www.wowinterface.com/downloads/download24924-RealMobHealth",

## CurseForge
Copy plugin url and the Project ID
* "https://www.curseforge.com/wow/addons/details-damage-meter-classic-wow?338524",

# Example user-config.json
```json
{
  "schema_version": "1.0",
  "plugins": [
    "file:///home/test/Downloads/SimpleEnergyBar-v1.0.11.zip",
    "https://bitbucket.org/yunohu/reciperadarclassic.git",
    "git@github.com:Hoizame/AtlasLootClassic.git",
    "https://github.com/joev/MoncaiCompare_Classic.git",
    { "path": "https://gitlab.com/bloodline_/widequestlog.git", "git_ref": "v1.5" },
    "https://www.wowinterface.com/downloads/download25202-InFlightClassic"
  ]
}
```

# Roadmap
* [X] Platform specific autogenerated paths for config/temp files
* [X] Better progress updates
* [ ] Application logs to file (only if enabled or in debug mode?)
* [ ] Support more plugin sites
  * [X] Figure out a workaround for CurseForge downloads
  * [ ] legacy-wow.com
  * [ ] svn
  * [X] Github Releases API
    * [X] Has issues if they dont use the assets functionality and instead just post a zip
    * [ ] Ensure the version of the asset is for the correct version of wow
  * [ ] moar
* [ ] Have a list of known good plugins available and easy to install directly in the tool
  * [ ] Basic description
  * [ ] Screenshots if available
  * [ ] Tags / Categories
  * [ ] Search / Filter
  * [ ] Search curseforge

# UI Workflows
* Initial Run
  * Identify either a system-config.json or a user-config.json file is missing
  * If system-config.json is missing:
    * Prompt user that one was not found, and a new one is being generated
    * Attempt to find Interface/AddOns folder
    * If no plugin folder found, prompt user to ensure wow is installed or select where the folder is
    * Update config to plugin path
    * Scan folder to see if plugins already exist. If there is at least 1 folder already in the AddOns folder:
      * Notify user of folders
      * For now ask if it is okay to delete them? maybe rename the Addons folder and make a new one?
      * [FUTURE] Attempt to auto import existing plugins
  * If user-config.json is missing:
    * Prompt user that one was not found, and a new one is being generated. Allow user to pick a location for this file
    * Update system-config.json to user-config path
* Plugin Listing Screen
  * List existing plugins (showing name, version, last version checked time?, install date?)
  * Each plugin has a refresh button telling the app to check if there is an update
  * Each plugin has a force update button, deleting local files and always updating




# more stuff
## Install Workflow
1. Download zip from gitlab
2. Unzip
3. Double click (or cli execute) binary extracted
4. Prompt for admin (otherwise right click > run as admin for previous step)
5. Application installs itself and sets up configuration files
6. If WoW install folder cannot be found, prompt user. Allow user to skip step if they want, and tell them out to change it later
7. Prompt for user to be able to setup desktop files (this is different for all platforms and DE :/ . Maybe make a special case for windows/mac?)

## Normal CLI Usage
1. Assume the application is on the PATH
2. `wpm plugin update` or `wpm.exe plugin update`

## Normal UI Usage
1. Run desktop shortcut or run the application with no parameters
2. UI opens. Runs a query to get all known plugins (combine both user/system config files and report on generalize install status)
3. For each addon, run fetch current version. If the addon should be uninstalled delete it here
4. Once the update status for each plugin is known, display it on the UI
5. Download / Update each plugin

In the future, steps 4 and 5 can optionally not run on startup

6. Allow user to check for update and/or force update for any/all plugins
7. Periodically auto check for update (configuration?)

## System Usage
1. Via UI or CLI allow to schedule auto updater
2. Updater runs `cli plugin update` on a schedule
