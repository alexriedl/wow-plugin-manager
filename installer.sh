#!/usr/bin/env sh

echo "Installing WoW Plugin Manager (we will need admin privileges to do this)"
TEMPORARY_EXECUTABLE="$(mktemp)"

verify_last_command() {
  COMMAND="$1"
  STATUS="${2-:0}"
  if [ "${STATUS}" -ne "0" ]; then
    printf "ERROR: '%s' failed with exit code: %s\n\n" "${COMMAND}" "${STATUS}"
    exit "${STATUS}"
  fi
}

EXECUTABLE_BYTE_SIZE=REPLACE_ME__BYTE_SIZE
tail -c "${EXECUTABLE_BYTE_SIZE}" "$0" > "${TEMPORARY_EXECUTABLE}" 2> /dev/null
if [ "$?" -ne "0" ]; then
  tail "-${EXECUTABLE_BYTE_SIZE}c" "$0" > "${TEMPORARY_EXECUTABLE}" 2> /dev/null
  if [ "$?" -ne "0" ]; then
    echo "tail didn't work. This could be caused by exhausted disk space. Aborting."
    exit 1
  fi
fi

sudo chmod +x "${TEMPORARY_EXECUTABLE}"
verify_last_command "chmod +x ${TEMPORARY_EXECUTABLE}" "$?"

echo "Installing Application"
sudo "${TEMPORARY_EXECUTABLE}" install # TODO: This should also add it to the path for unix peoples
verify_last_command "${TEMPORARY_EXECUTABLE} install" "$?"

echo "Removing Temporary Files"
rm -f "${TEMPORARY_EXECUTABLE}"
verify_last_command "rm -f ${TEMPORARY_EXECUTABLE}" "$?"

echo "Setting Application Privileges"
sudo chmod 755 /opt/wpm/wpm
verify_last_command "chmod 755 /opt/wpm/wpm" "$?"

echo "Initializing the Application"
/opt/wpm/wpm install initialize
verify_last_command "wpm install initialize" "$?"

exit 0
