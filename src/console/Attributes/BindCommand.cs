using System;

namespace Console.Attributes
{
	[AttributeUsage(AttributeTargets.Class, AllowMultiple = true, Inherited = false)]
	public class BindCommand : Attribute
	{
		public string Name { get; set; }
		public string Description { get; set; }

		public BindCommand(string name, string description)
		{
			Name = name;
			Description = description;
		}
	}
}
