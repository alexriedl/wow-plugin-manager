using System;

namespace Console.Attributes
{
	[AttributeUsage(AttributeTargets.Class, AllowMultiple = false, Inherited = false)]
	public class BindDefaultSubcommand : Attribute
	{
		public string Name { get; set; }

		public BindDefaultSubcommand(string name)
		{
			Name = name;
		}
	}
}
