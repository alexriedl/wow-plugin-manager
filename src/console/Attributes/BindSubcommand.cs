using System;

namespace Console.Attributes
{
	[AttributeUsage(AttributeTargets.Method, AllowMultiple = true, Inherited = false)]
	public class BindSubcommand : Attribute
	{
		public string Name { get; set; }
		public string Description { get; set; }

		public BindSubcommand(string name, string description)
		{
			Name = name;
			Description = description;
		}
	}
}
