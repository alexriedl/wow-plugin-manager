using Console.Attributes;
using Core;
using Core.Notifiers;
using Core.UseCases;
using NLog;
using Newtonsoft.Json.Serialization;
using Newtonsoft.Json;
using System.Linq;
using System.Threading.Tasks;
using System.Threading;

namespace Console.Commands
{
	[BindCommand("plugin", "Commands to manage plugins")]
	public class PluginCommands : ICommand
	{
		private readonly JsonSerializerSettings jsonSettings;
		private readonly ConfigUseCases configUseCases;
		private readonly UpdatePluginsUseCase updatePluginsUseCase;

		private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

		public PluginCommands(ConfigUseCases configUseCases, UpdatePluginsUseCase updatePluginsUseCase)
		{
			this.configUseCases = configUseCases;
			this.updatePluginsUseCase = updatePluginsUseCase;

			jsonSettings = new JsonSerializerSettings
			{
				ContractResolver = new DefaultContractResolver
				{
					NamingStrategy = new SnakeCaseNamingStrategy(),
				},
				Formatting = Formatting.Indented,
			};
		}

		[BindSubcommand("test", "test command")]
		public int test()
		{
			Logger.Info("TEST LOG");
			return 0;
		}

		[BindSubcommand("add", "Add a new plugin")]
		public async Task<int> Add(string pluginUrl, string gitRef = null)
		{
			var result = await configUseCases.AddPlugin(pluginUrl, gitRef);
			switch(result)
			{
				case ConfigUseCases.AddPluginResult.PluginAdded:
				{
					System.Console.WriteLine("Plugin Added");
				} break;
				case ConfigUseCases.AddPluginResult.InvalidUrl:
				{
					System.Console.WriteLine("Invalid Url");
				} break;
				case ConfigUseCases.AddPluginResult.MissingUrl:
				{
					System.Console.WriteLine("Missing Url");
				} break;
				case ConfigUseCases.AddPluginResult.PluginAlreadyExists:
				{
					System.Console.WriteLine("Plugin Already Exists");
				} break;
				case ConfigUseCases.AddPluginResult.FailedToAddPlugin:
				{
					System.Console.WriteLine("Failed to add plugin");
				} break;
				default:
				{
					System.Console.WriteLine("UNKNOWN RESULT");
				} return -1;
			}

			return 0;
		}

		[BindSubcommand("list", "Return a list of the plugins that are installed")]
		public async Task<int> List(int page = 1, int limit = 20)
		{
			if(page < 1) page = 1;
			if(limit < 1) limit = 1;

			var plugins = await configUseCases.GetPluginLinkList();
			var pagedPlugins = plugins.Skip((page - 1) * limit).Take(limit);
			System.Console.WriteLine(JsonConvert.SerializeObject(pagedPlugins, jsonSettings));
			return 0;
		}

		[BindSubcommand("update", "Updates all plugins that are out of date")]
		public async Task<int> Update(CancellationToken token = default(CancellationToken))
		{
			var notifierFactory = new PluginStatusNotifierFactory();
			// TODO: Having this better configurable.
			if(OsInfo.IsWindows()) notifierFactory.RegisterNotifier<LogPluginStatusNotifier>();
			else notifierFactory.RegisterNotifier<ConsolePluginStatusNotifier>();

			await updatePluginsUseCase.UpdatePlugins(notifierFactory, token);
			System.Console.WriteLine("Plugins Updated");
			// TODO: We probably want a response from the updater, and report to the console
			return 0;
		}
	}
}
