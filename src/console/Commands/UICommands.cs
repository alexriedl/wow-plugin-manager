using Console.Attributes;
using Core.UseCases;

namespace Console.Commands
{
	[BindCommand("ui", "Commands that control the UI")]
	[BindCommand("interface", "Commands that control the UI (alias for 'ui')")]
	[BindDefaultSubcommand("launch")]
	public class UICommands : ICommand
	{
		private readonly WpmSetupUseCases setupUseCases;

		public UICommands(WpmSetupUseCases setupUseCases)
		{
			this.setupUseCases = setupUseCases;
		}

		[BindSubcommand("launch", "Launch the UI for WPM")]
		public int Launch()
		{
			global::UI.Program.StartUIAndBlock();
			return 0;
		}
	}
}
