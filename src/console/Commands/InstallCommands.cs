using Console.Attributes;
using Core.UseCases;
using Core;

namespace Console.Commands
{
	[BindCommand("install", "Command group to help with installing WPM")]
	[BindDefaultSubcommand("install")]
	public class InstallCommands : ICommand
	{
		private readonly WpmSetupUseCases setupUseCases;

		public InstallCommands(WpmSetupUseCases setupUseCases)
		{
			this.setupUseCases = setupUseCases;
		}

		[BindSubcommand("install", "Install WPM")]
		public int Install()
		{
			var result = setupUseCases.Install();
			return result.Match(
				success =>
				{
					System.Console.WriteLine("Install Successful");
					return 0;
				},
				failure =>
				{
					System.Console.WriteLine(failure.Reason);
					return 1;
				}
			);
		}

		[BindSubcommand("initialize", "Initialize WPM")]
		public int Initialize()
		{
			var result = setupUseCases.Initialize();
			return result.Match(
				success =>
				{
					System.Console.WriteLine("Initializing Complete");
					return 0;
				},
				failure =>
				{
					System.Console.WriteLine(failure.Reason);
					return 1;
				}
			);
		}

		[BindSubcommand("full-install", "Install and initialize WPM. This should only be used if you can install as the same user who plans on running this application")]
		public int FullInstall()
		{
			var installResult = Install();
			if(installResult != 0) return installResult;
			return Initialize();
		}

		[BindSubcommand("info", "Show path information about this WPM install")]
		public int Info()
		{
			System.Console.WriteLine($"Wpm Install Directory: {OsInfo.GetDefaultInstallDirectory()}");
			System.Console.WriteLine($"Wpm User Config: {UserConfig.FullFileName}");
			System.Console.WriteLine($"Wpm System Config: {SystemConfig.FullFileName}");

			return 0;
		}
	}
}
