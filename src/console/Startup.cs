using System;
using Console.Commands;
using Microsoft.Extensions.DependencyInjection;

namespace Console
{
	public static class Startup
	{
		public static IServiceProvider RegisterDependencies(IServiceCollection services = null)
		{
			if(services == null) services = new ServiceCollection();

			services.AddTransient<ICommand, PluginCommands>();
			services.AddTransient<ICommand, InstallCommands>();
			services.AddTransient<ICommand, UICommands>();

			Core.Startup.RegisterDependencies(services);

			return services.BuildServiceProvider();
		}
	}
}
