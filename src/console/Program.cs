﻿using Console.Attributes;
using Console.Commands;
using Core.Extensions;
using Core;
using Microsoft.Extensions.DependencyInjection;
using NLog;
using OneOf;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using System;

namespace Console
{
	class Program
	{
		public static async Task Main(string[] args)
		{
			var rawEnableLogs = Environment.GetEnvironmentVariable("WPM_ENABLE_LOGS");
			if(!bool.TryParse(rawEnableLogs, out var enableLogs) || !enableLogs)
			{
				LogManager.GlobalThreshold = LogLevel.Off;
			}

			var exeLocation = System.IO.Path.GetDirectoryName(System.Diagnostics.Process.GetCurrentProcess().MainModule.FileName);
			var installLocation = System.IO.Path.GetFullPath(OsInfo.GetDefaultInstallDirectory());
			var looksLikeThisExeIsInstalled = installLocation.ToLower() == exeLocation.ToLower();

			var forcePrompt = false;
			if(args.Contains("--force-prompt"))
			{
				args = args.Where(s => s != "--force-prompt").ToArray();
				forcePrompt = true;
			}

			if(OsInfo.IsWindows() && args.Length == 0)
			{
				if(!looksLikeThisExeIsInstalled)
				{
					args = new string[] { "install", "full-install" };
					forcePrompt = true;
				}
			}

			if(args.Length == 0 && looksLikeThisExeIsInstalled)
			{
				args = new string[] { "plugin", "update" };
			}

			var exitCode = await EntryPoint(args);
			Environment.ExitCode = exitCode;

			if(forcePrompt)
			{
				System.Console.WriteLine("Press Enter to Continue");
				System.Console.ReadLine();
			}
		}

		public static async Task<int> EntryPoint(string[] args)
		{
			var commands = RegisterDependenciesAndGetAvailableCommands();

			if(args.Length == 0 || args[0].Equals("--help") || args[0].Equals("-h"))
			{
				CommandInfoPrinter.PrintApplicationUsage(commands);
				return 1;
			}

			var argsConsumed = 1;
			var command = args[0];
			var foundCommand = commands.FirstOrDefault(c => c.BoundCommandNames.Any(a => a.Name == command));
			if(foundCommand == null)
			{
				System.Console.WriteLine("Command Not Found\n");
				CommandInfoPrinter.PrintApplicationUsage(commands);
				return 1;
			}

			var subcommands = foundCommand.CommandType.GetMethods()
				.Select(methodInfo => new SubcommandWrapper
				{
					Method = methodInfo,
					Attributes = (BindSubcommand[])methodInfo.GetCustomAttributes(typeof(BindSubcommand), true),
				})
				.Where(s => s.Attributes.Length > 0)
				.ToArray();

			var subcommand = args.Length > 1 ? args[1] : foundCommand.DefaultSubcommand?.Name;
			if(subcommand != null && subcommand[0] == '-') subcommand = foundCommand.DefaultSubcommand?.Name;
			else argsConsumed += 1;

			var foundSubcommand = subcommands.FirstOrDefault(s => s.Attributes.Any(a => a.Name == subcommand));
			if(foundSubcommand == null)
			{
				System.Console.WriteLine($"Subcommand '{subcommand}' Not Found\n");
				CommandInfoPrinter.PrintCommandUsage(subcommands);
				return 1;
			}

			var commandArgs = args.Skip(argsConsumed).ToArray();
			if(commandArgs.Contains("--help") || commandArgs.Contains("-h"))
			{
				var subcommmandInfo = foundSubcommand.Attributes.FirstOrDefault(a => a.Name == subcommand);
				if(subcommmandInfo == null)
				{
					System.Console.WriteLine("No help information found for command.");
				}
				else
				{
					System.Console.WriteLine(subcommmandInfo.Name);
					System.Console.WriteLine(subcommmandInfo.Description);
					// TODO: Get and print parameter info
					// System.Console.WriteLine(subcommmandInfo.ParameterInfo);
				}
				return 0;
			}

			var result = await CommandRunner.InvokeCommand(foundCommand.CommandInstance, foundSubcommand.Method, commandArgs);
			return result;
		}

		private static CommandWrapper[] RegisterDependenciesAndGetAvailableCommands()
		{
			var serviceProvider = Startup.RegisterDependencies();
			var commands = serviceProvider.GetServices<ICommand>()
				.Select(c =>
				{
					var type = c.GetType();
					return new CommandWrapper
					{
						CommandInstance = c,
						CommandType = type,
						BoundCommandNames = (BindCommand[])Attribute.GetCustomAttributes(type, typeof(BindCommand)),
						DefaultSubcommand = (BindDefaultSubcommand)Attribute.GetCustomAttribute(type, typeof(BindDefaultSubcommand))
					};
				})
				.Where(c => c.BoundCommandNames.Length > 0)
				.ToArray();
			return commands;
		}
	}

	public class CommandWrapper
	{
		public ICommand CommandInstance { get; set; }
		public Type CommandType { get; set; }
		public BindCommand[] BoundCommandNames { get; set; }
		public BindDefaultSubcommand DefaultSubcommand { get; set; }
	}

	public class SubcommandWrapper
	{
		public MethodInfo Method { get; set; }
		public BindSubcommand[] Attributes { get; set; }
	}

	public class CommandInfoPrinter
	{
		public static void PrintCommandUsage(SubcommandWrapper[] subcommands)
		{
			var commandLines = subcommands
				.SelectMany(c => c.Attributes)
				.Select(c => new { Name = c.Name.ConvertTitleCaseToSnakeCase(), Description = c.Description })
				.OrderBy(c => c.Name)
				.Select(c => $"{c.Name} - {c.Description}");

			var commandList = string.Join('\n', commandLines);
			System.Console.WriteLine($"Available Subcommands:\n\n{commandList}");
		}

		public static void PrintApplicationUsage(CommandWrapper[] commands)
		{
			var commandLines = commands
				.SelectMany(c => c.BoundCommandNames)
				.Select(c => new { Name = c.Name.ConvertTitleCaseToSnakeCase(), Description = c.Description })
				.OrderBy(c => c.Name)
				.Select(c => $"{c.Name} - {c.Description}");

			var commandList = string.Join('\n', commandLines);
			System.Console.WriteLine($"Available Commands:\n\n{commandList}");
		}
	}

	public class CommandRunner
	{
		public static Task<int> InvokeCommand(ICommand instance, MethodInfo subcommand, string[] cliParameters)
		{
			return InvokeCommand(instance, subcommand, cliParameters, failedToMapParametersResult: -1);
		}

		public static Task<T> InvokeCommand<T>(ICommand instance, MethodInfo subcommand, string[] cliParameters, T failedToMapParametersResult)
		{
			var namedParameters = CommandRunner.MapCliArgsToDictionary(cliParameters);
			var parameters = CommandRunner.MapNamedParametersToMethodParameters(subcommand, namedParameters);

			return parameters.Match(
				success => InvokeMethodAndAwaitIfNeeded<T>(instance, subcommand, success.Parameters),
				failure =>
				{
					System.Console.WriteLine("Failed to map all required parameters");
					System.Console.WriteLine("Errors:");
					System.Console.WriteLine(string.Join("\n", failure.Errors));
					return Task.FromResult(failedToMapParametersResult);
				}
			);
		}

		private static async Task<T> InvokeMethodAndAwaitIfNeeded<T>(object instance, MethodInfo method, object[] parameters)
		{
			var rawResult = method.Invoke(instance, parameters);

			T result;
			if(rawResult is Task)
			{
				result = await (rawResult as Task<T>);
			}
			else
			{
				result = (T)rawResult;
			}

			return result;
		}

		private static Dictionary<string, object> MapCliArgsToDictionary(string[] commandArgs)
		{
			// TODO: Support short flags? What about multiple short flags together? -r -f or -rf
			// TODO: Support implicit parameters? like the final param in rm: `rm -rf /home`. Which parameter in the function would /home be assigned to?
			var parameters = new Dictionary<string, object>();
			var parameterIndex = 0;
			while(parameterIndex < commandArgs.Length)
			{
				var parameterName = commandArgs[parameterIndex];
				if(!parameterName.StartsWith("--"))
				{
					// TODO: Invalid parameter.... do something smarter here
					// To support short flags and implicit parameters this has to change
					continue;
				}
				parameterIndex += 1;

				object parameterValue;
				if(parameterIndex < commandArgs.Length && !commandArgs[parameterIndex].StartsWith("--"))
				{
					parameterValue = commandArgs[parameterIndex];
					parameterIndex += 1;
				}
				else
				{
					if(parameterName.StartsWith("--no-"))
					{
						parameterValue = false;
						parameterName = parameterName.Replace("--no-", string.Empty);
					}
					else parameterValue = true;
				}
				var finalParameterName = parameterName.ConvertSnakeCaseToPascelCase();
				if(parameters.ContainsKey(finalParameterName)) parameters[finalParameterName] = parameterValue;
				else parameters.Add(finalParameterName, parameterValue);
			}

			return parameters;
		}

		private static MapParametersResult MapNamedParametersToMethodParameters(MethodInfo method, IDictionary<string, object> namedParameters)
		{
			var validParams = method.GetParameters();
			var paramNames = validParams.Select(p => p.Name).ToArray();
			var parameters = new object[paramNames.Length];
			for(var paramIndex = 0; paramIndex < parameters.Length; ++paramIndex)
			{
				parameters[paramIndex] = validParams[paramIndex].HasDefaultValue
					? validParams[paramIndex].DefaultValue
					: Type.Missing;
			}
			foreach(var item in namedParameters)
			{
				var paramName = item.Key;
				var paramIndex = Array.IndexOf(paramNames, paramName);
				if(paramIndex < 0)
				{
					// TODO: Blindly skipping invalid params is probably the wrong strategy
					// TODO: As an error to the user, we probably should display exactly what they typed, not a transformed version of it.
					System.Console.WriteLine($"Invalid parameter {paramName}. Skipping...");
					continue;
				}

				var paramType = validParams[paramIndex].ParameterType;
				// TODO: If we fail to cast the parameter the following line will fail and we probably should handle this better..
				parameters[paramIndex] = Convert.ChangeType(item.Value, paramType);
			}

			var missingParameters = parameters.Where(p => p == Type.Missing).ToArray();
			if(missingParameters.Any())
			{
				return new MapParametersResult.Failure
				{
					Errors = missingParameters
						.Select((parameter, index) => $"Missing parameter: {validParams[index].ParameterType.Name} {validParams[index].Name}")
						.ToArray()
				};
			}

			return new MapParametersResult.Success
			{
				Parameters = parameters
			};
		}

		public abstract class MapParametersResult : OneOfBase<MapParametersResult.Success, MapParametersResult.Failure>
		{
			public class Success : MapParametersResult { public object[] Parameters { get; set; } }
			public class Failure : MapParametersResult { public string[] Errors { get; set; } }
		}
	}
}
