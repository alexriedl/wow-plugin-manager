using System.IO;
using System.Runtime.InteropServices;
using System;

namespace Core
{
	public static class OsInfo
	{
		public static bool IsWindows() => RuntimeInformation.IsOSPlatform(OSPlatform.Windows);
		public static bool IsMacOS() => RuntimeInformation.IsOSPlatform(OSPlatform.OSX);
		public static bool IsLinux() => RuntimeInformation.IsOSPlatform(OSPlatform.Linux);

		public static string GetDefaultInstallDirectory()
		{
			if(IsWindows()) return Path.Combine("c:/", "Program Files", "WPM");
			if(IsLinux()) return Path.Combine("/", "opt", "wpm");
			if(IsMacOS()) return Path.Combine("/", "opt", "wpm");

			throw new System.InvalidOperationException("Unknown Operating System");
		}

		public static string GetDefaultConfigFilesDirectory()
		{
			var userDataFolder = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData, Environment.SpecialFolderOption.Create);
			return Path.Combine(userDataFolder, "wpm");
		}

		public static string GetDefaultTemporaryFilesDirectory()
		{
			return Path.Combine(GetDefaultConfigFilesDirectory(), "temporary_files");
		}

		public static string GetDefaultCliExecutableName()
		{
			if(IsWindows()) return "wpm.exe";
			return "wpm";
		}

		public static string[] GetPossibleWowInstallDirectory()
		{
			if(IsWindows()) return new string[]
			{
				Path.Combine("c:/", "Program Files", "World of Warcraft"),
				Path.Combine("c:/", "Program Files (x86)", "World of Warcraft"),
			};
			if(IsMacOS()) return new string[]
			{
				Path.Combine("/", "Applications", "World of Warcraft"),
			};
			if(IsLinux()) return new string[]
			{
			};

			return new string[0];
		}

		public static string GetWowAddOnDirectory(string wowInstallDirectory, string wowVersionSlug = "_classic_")
		{
			return Path.Combine(wowInstallDirectory, wowVersionSlug, "Interface", "AddOn");
		}
	}
}
