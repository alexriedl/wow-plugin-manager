using Core.Components.Installers;
using Core.Components;
using Core.UseCases;
using Microsoft.Extensions.DependencyInjection;

namespace Core
{
	public static class Startup
	{
		public static void RegisterDependencies(IServiceCollection services)
		{
			services.AddHttpClient();

			services.AddTransient<ExtractZipComponent>();
			services.AddTransient<InstallPluginComponent>();
			services.AddTransient<UninstallPluginComponent>();
			services.AddTransient<ZipUrlInstallerComponent>();

			services.AddTransient<PluginInstaller, CurseInstaller>();
			services.AddTransient<PluginInstaller, GitInstaller>();
			services.AddTransient<PluginInstaller, GithubApiInstaller>();
			services.AddTransient<PluginInstaller, WowInterfaceInstaller>();

			services.AddTransient<ConfigUseCases>();
			services.AddTransient<UpdatePluginsUseCase>();
			services.AddTransient<GetPluginListUseCase>();
			services.AddTransient<WpmSetupUseCases>();
		}
	}
}
