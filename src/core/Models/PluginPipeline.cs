/*
namespace Core.Models
{
	public class PluginPipeline
	{
		public PluginVersionChecker VersionChecker { get; set; }
		public PluginDownloader Downloader { get; set; }
		public PluginExtractor Extractor { get; set; }
		public PluginInstaller Installer { get; set; }
	}

	public abstract class PluginVersionChecker
	{
	}

	public abstract class PluginDownloader
	{
	}

	public abstract class PluginExtractor
	{
	}

	public abstract class PluginInstaller
	{
	}

	public enum PluginStatus
	{
		NotInstalled,
		OutOfDate,
		MissingLocalFiles,
		UpToDate,
	}

	public class Holder
	{
		public PluginStatus GetPluginStatus(PluginPipeline plugin)
		{
		}

		public bool PipelineIsValid(PluginPipeline plugin)
		{
			if(plugin.VersionChecker == null) return false;
			if(plugin.PluginDownloader == null) return false; // NOTE: Local installer probably doesnt need an downloader...
			if(plugin.PluginInstaller == null) return false;
			return true;
		}

		public void FullPipeline(PluginPipeline plugin)
		{
			if(!PipelineIsValid(plugin))
			{
				return;
			}

			var currentStatus = GetPluginStatus(plugin);
			if(currentStatus == PluginStats.UpToDate)
			{
				return;
			}

		}
	}
}
*/
