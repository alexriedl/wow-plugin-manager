using Core.Components.Installers;
using Core.Extensions;
using Core.Notifiers;
using NLog;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Threading;
using System;

namespace Core.UseCases
{
	public class UpdatePluginsUseCase
	{
		private static readonly Logger Logger = LogManager.GetCurrentClassLogger();
		private readonly PluginInstaller[] installers;

		public UpdatePluginsUseCase(IEnumerable<PluginInstaller> installers)
		{
			this.installers = installers.ToArray();
		}

		public async Task UpdatePlugins(
			PluginStatusNotifierFactory notifierFactory,
			CancellationToken token = default(CancellationToken)
		)
		{
			Config.EnsureConfigsExist();
			var systemConfig = Config.LoadSystemConfig();
			var userConfig = Config.LoadUserConfig();

			var removedPlugins = systemConfig.PluginInfo.Keys.Except(userConfig.Plugins.Select(p => p.Path)).ToArray();
			foreach(var removedPlugin in removedPlugins)
			{
				DeletePluginFolders(systemConfig, removedPlugin);
				systemConfig.PluginInfo.Remove(removedPlugin);

				var notifier = notifierFactory.Create(removedPlugin);
				notifier.StatusChanged(Status.Removed, "Plugin removed from user config");
			}

			var parallelTasks = 5;
			var semaphore = new SemaphoreSlim(initialCount: parallelTasks, maxCount: parallelTasks);
			await Task.WhenAll(userConfig.Plugins.Select(p => UpdatePlugin(p, systemConfig, notifierFactory, semaphore, token)));

			Config.Save(systemConfig);
		}

		private void DeletePluginFolders(SystemConfig systemConfig, string pluginKey)
		{
			var oldPluginFolders = systemConfig.PluginInfo.Find(pluginKey)?.LocalFolders;
			if(oldPluginFolders != null && oldPluginFolders.Length > 0)
			{
				Logger.Info($"Deleting old plugin folders: [{string.Join(",", oldPluginFolders)}]");
				foreach(var pluginName in oldPluginFolders)
				{
					var path = Path.Combine(systemConfig.InterfaceFolder, pluginName);
					if(Directory.Exists(path))
					{
						Directory.Delete(path, recursive: true);
					}
				}
			}
		}

		private async Task UpdatePlugin(
			UserConfig.Info pluginInfo,
			SystemConfig systemConfig,
			PluginStatusNotifierFactory notifierFactory,
			SemaphoreSlim semaphore,
			CancellationToken token = default(CancellationToken)
		)
		{
			var pluginKey = pluginInfo.Path;
			var systemInfo = systemConfig.PluginInfo.FindOrNew(pluginKey);
			var notifier = notifierFactory.Create(pluginKey);
			var installer = installers.FirstOrDefault(i => i.CanDownload(pluginInfo.Path));
			if(installer == null)
			{
				notifier.StatusChanged(Status.Failure, "No installer found");
				return;
			}
			try
			{
				await semaphore.WaitAsync();
				await installer.UpdatePlugin(pluginInfo, systemInfo, systemConfig.InterfaceFolder, notifier, token);
			}
			catch(Exception e)
			{
				Logger.Error(e);
				notifier.StatusChanged(Status.Failure, "Fatal Error");
			}
			finally
			{
				semaphore.Release();
			}
		}
	}
}
