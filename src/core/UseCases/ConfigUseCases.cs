using Core.Extensions;
using System.Linq;
using System.Threading.Tasks;

namespace Core.UseCases
{
	public class ConfigUseCases
	{
		public class PluginLinkListItem
		{
			public string Url { get; set; }
			public SystemConfig.Info PluginInfo { get; set; } // TODO: This feels wierd using this model
		}

		public Task<PluginLinkListItem[]> GetPluginLinkList()
		{
			Config.EnsureConfigsExist();
			var systemConfig = Config.LoadSystemConfig();
			var userConfig = Config.LoadUserConfig();
			var result = userConfig.Plugins.Select(p => new PluginLinkListItem
			{
				Url = p.Path,
				PluginInfo = systemConfig?.PluginInfo?.Find(p.Path)
				// TODO: We moved branch to user config, this needs to be updated to include that information now
			})
			.ToArray();

			return Task.FromResult(result);
		}

		public Task<AddPluginResult> AddPlugin(string url, string gitRef)
		{
			if(url == null) return Task.FromResult(AddPluginResult.MissingUrl);
			url = url?.Trim();

			Config.EnsureConfigsExist();
			var userConfig = Config.LoadUserConfig();
			var existingPlugin = userConfig.Plugins.Where((p) => p.Path == url).FirstOrDefault();
			if(existingPlugin != null)
			{
				if(gitRef == existingPlugin.VersionRef) return Task.FromResult(AddPluginResult.PluginAlreadyExists);
				else existingPlugin.VersionRef = gitRef;
			}
			else
			{
				userConfig.Plugins = userConfig.Plugins
					.Union(new UserConfig.Info[] { new UserConfig.Info { Path = url, VersionRef = gitRef } })
					.ToArray();
			}

			if(Config.Save(userConfig))
			{
				return Task.FromResult(AddPluginResult.PluginAdded);
			}
			else
			{
				return Task.FromResult(AddPluginResult.FailedToAddPlugin);
			}
		}

		public enum AddPluginResult
		{
			PluginAdded,
			InvalidUrl,
			MissingUrl,
			PluginAlreadyExists,
			FailedToAddPlugin,
		}
	}
}
