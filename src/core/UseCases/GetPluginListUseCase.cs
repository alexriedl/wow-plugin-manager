using Core.Components.Installers;
using NLog;
using System.Collections.Generic;
using System.Linq;

namespace Core.UseCases
{
	public class GetPluginListUseCase
	{
		private static readonly Logger Logger = LogManager.GetCurrentClassLogger();
		private readonly PluginInstaller[] installers;

		public GetPluginListUseCase(IEnumerable<PluginInstaller> installers)
		{
			this.installers = installers.ToArray();
		}

		public PluginStatus[] GetPluginList()
		{
			return GenericGetPluginList<PluginStatus>();
		}

		private T CreatePluginStatus<T>(string key, string status) where T : PluginStatus, new()
		{
			var title = key;
			var installer = installers.FirstOrDefault(i => i.CanDownload(key));
			if(installer != null) title = installer.GetShortTitle(key);

			return new T
			{
				Key = key,
				Title = title,
				Status = status,
			};
		}

		public T[] GenericGetPluginList<T>() where T : PluginStatus, new()
		{
			Config.EnsureConfigsExist();
			var systemConfig = Config.LoadSystemConfig();
			var userConfig = Config.LoadUserConfig();

			var removedPlugins = systemConfig.PluginInfo.Keys
				.Except(userConfig.Plugins.Select(p => p.Path))
				.Select(k => CreatePluginStatus<T>(k, "Marked for removal"));

			var newPlugins = userConfig.Plugins
				.Select(p => p.Path)
				.Except(systemConfig.PluginInfo.Keys)
				.Select(k => CreatePluginStatus<T>(k, "Not installed yet"))
				.ToArray();

			var normalPlugins = userConfig.Plugins
				.Select(p => p.Path)
				.Where(p => !newPlugins.Any(n => n.Title == p))
				.Select(k => CreatePluginStatus<T>(k, "Active"));

			var result = removedPlugins
			.Union(newPlugins)
			.Union(normalPlugins)
			.OrderBy(p => p.Title)
			.ToArray();
			return result;
		}

		public class PluginStatus
		{
			public virtual string Key { get; set; }
			public virtual string Title { get; set; }
			public virtual string Status { get; set; }
		}
	}
}
