using Core.Components;
using NLog;
using OneOf;
using System.IO;
using System.Runtime.InteropServices;
using System.Security.Principal;

namespace Core.UseCases
{
	public class InstallWpmResult : OneOfBase<InstallWpmResult.Success, InstallWpmResult.Failure>
	{
		public class Success : InstallWpmResult {}
		public class Failure : InstallWpmResult { public string Reason { get; set; } }
	}

	public class RunningAsAdminResult : OneOfBase<RunningAsAdminResult.Admin, RunningAsAdminResult.NotAdmin>
	{
		public class Admin : RunningAsAdminResult {}
		public class NotAdmin : RunningAsAdminResult {}
	}

	public class WpmSetupUseCases
	{
		private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

		[DllImport("libc")]
		public static extern uint getuid();

		public RunningAsAdminResult IsRunningAsAdmin()
		{
			if(OsInfo.IsWindows())
			{
				using(var identity = WindowsIdentity.GetCurrent())
				{
					var principal = new WindowsPrincipal(identity);
					if(principal.IsInRole(WindowsBuiltInRole.Administrator))
					{
						return new RunningAsAdminResult.Admin();
					}
				}
			}
			else if(getuid() == 0)
			{
				return new RunningAsAdminResult.Admin();
			}

			return new RunningAsAdminResult.NotAdmin();
		}

		public InstallWpmResult Install()
		{
#if DEBUG
			Logger.Error($"Not allowed to install a debug version of this application. You are only allowed to install a Release version because it will come with all of its dependencies in a single file.");
			return new InstallWpmResult.Failure
			{
				Reason = "Not allowed to install debug version of this application"
			};
#else
			var isAdminResult = IsRunningAsAdmin();
			if(isAdminResult is RunningAsAdminResult.NotAdmin)
			{
				return new InstallWpmResult.Failure
				{
					Reason = "Must run the installer as an administrator"
				};
			}

			var targetInstallDirectory = OsInfo.GetDefaultInstallDirectory();
			var executableFullName = System.Diagnostics.Process.GetCurrentProcess().MainModule.FileName;
			var runningDirectory = Path.GetDirectoryName(executableFullName);

			if(runningDirectory == targetInstallDirectory)
			{
				return new InstallWpmResult.Failure
				{
					Reason = "Unable to install when running from the install directory"
				};
			}

			if(!Directory.Exists(targetInstallDirectory))
			{
				Logger.Info($"Creating install directory {targetInstallDirectory}");
				Directory.CreateDirectory(targetInstallDirectory);
			}

			var targetInstallFullName = Path.Combine(targetInstallDirectory, OsInfo.GetDefaultCliExecutableName());
			// TODO: Probably need to do this in a try catch to see if it fails
			File.Copy(executableFullName, targetInstallFullName, overwrite: true);

			return new InstallWpmResult.Success();
#endif
		}

		public InstallWpmResult Initialize()
		{
			var configDirectory = new DirectoryInfo(OsInfo.GetDefaultConfigFilesDirectory());
			if(!configDirectory.Exists)
			{
				configDirectory.Create();
			}

			var temporaryDirectory = new DirectoryInfo(OsInfo.GetDefaultTemporaryFilesDirectory());
			if(!temporaryDirectory.Exists)
			{
				temporaryDirectory.Create();
			}

			if(!File.Exists(SystemConfig.FullFileName))
			{
				var oldConfigLocation = Path.Combine(OsInfo.GetDefaultInstallDirectory(), SystemConfig.FileName);
				if(File.Exists(oldConfigLocation))
				{
					Logger.Info($"No System Config found at '{SystemConfig.FullFileName}', but found one at {oldConfigLocation}. Copying it to the new spot");
					var config = Config.LoadSystemConfig(oldConfigLocation);
					Config.Save(config);
				}
				else
				{
					Logger.Info($"No System Config found at '{SystemConfig.FullFileName}', creating a new one.");

					string addonsFolder = null;
					foreach(var testFolder in OsInfo.GetPossibleWowInstallDirectory())
					{
						if(Directory.Exists(testFolder))
						{
							addonsFolder = OsInfo.GetWowAddOnDirectory(testFolder);
						}
					}

					if(addonsFolder == null)
					{
						Logger.Error($"No WoW installation found. Looked in the following locations: [{string.Join(", ", OsInfo.GetPossibleWowInstallDirectory())}]");
						Logger.Info("If WoW is installed, you must manually modify the system-config.json file and set the InterfaceFolder property to the full path of the 'Interface/AddOns' directory");
					}

					Config.Save(new SystemConfig
					{
						InterfaceFolder = addonsFolder
					});
				}
			}

			if(!File.Exists(UserConfig.FullFileName))
			{
				var oldConfigLocation = Path.Combine(OsInfo.GetDefaultInstallDirectory(), UserConfig.FileName);
				if(File.Exists(oldConfigLocation))
				{
					Logger.Info($"No System Config found at '{UserConfig.FullFileName}', but found one at {oldConfigLocation}. Copying it to the new spot");
					var config = Config.LoadUserConfig(oldConfigLocation);
					Config.Save(config);
					Logger.Info($"!!! Use the following config file to add/modify addons from now on: {UserConfig.FullFileName}");
				}
				else
				{
					Logger.Info($"No User Config found at '{UserConfig.FullFileName}', creating a new one.");
					Config.Save(new UserConfig());
				}
			}

			// TODO: This should prompt the user if they want to do this instead
			Logger.Info($"Creating Shortcuts");
			System.Console.WriteLine("Creating Shortcuts");
			ShortcutComponent.CreateDesktopShortcut(UserConfig.FullFileName, "WPM Plugins", "Add/Remove WoW Plugins using WPM");
			ShortcutComponent.CreateDesktopShortcut(Path.Combine(OsInfo.GetDefaultInstallDirectory(), OsInfo.GetDefaultCliExecutableName()), "Update WoW Plugins", "Add/Remove WoW Plugins using WPM", "--force-prompt");

			return new InstallWpmResult.Success();
		}
	}
}
