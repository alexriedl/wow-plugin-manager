using Core.Notifiers;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Threading;

namespace Core.Components.Installers
{
	public abstract class PluginInstaller
	{
		public abstract bool CanDownload(string pluginKey);
		public abstract string GetShortTitle(string pluginKey);
		public abstract Task UpdatePlugin(
			UserConfig.Info pluginInfo,
			SystemConfig.Info systemInfo,
			string addonFolder,
			PluginStatusNotifier notifier,
			CancellationToken token = default(CancellationToken));

		public bool AllInstalledFoldersExist(SystemConfig.Info systemInfo, string addonFolder)
		{
			if(!systemInfo.LocalFolders.Any())
			{
				return false;
			}

			foreach(var folder in systemInfo.LocalFolders)
			{
				var path = Path.Combine(addonFolder, folder);
				if(!Directory.Exists(path)) return false;
			}

			return true;
		}
	}
}
