using Core.Notifiers;
using NLog;
using Newtonsoft.Json;
using System.Linq;
using System.Net.Http;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Threading;

namespace Core.Components.Installers
{
	public class GithubApiInstaller : PluginInstaller
	{
		private static readonly Logger Logger = LogManager.GetCurrentClassLogger();
		private readonly IHttpClientFactory clientFactory;
		private readonly ZipUrlInstallerComponent zipUrlInstallerComponent;

		public GithubApiInstaller(IHttpClientFactory clientFactory, ZipUrlInstallerComponent zipUrlInstallerComponent)
		{
			this.clientFactory = clientFactory;
			this.zipUrlInstallerComponent = zipUrlInstallerComponent;
		}

		private (string projectPath, string projectName) GetProjectInfoFromPath(string path)
		{
			var regex = new Regex(@"^https:\/\/github.com\/(.*)$", RegexOptions.Compiled);
			var matches = regex.Matches(path);
			if(matches.Count <= 0)
			{
				return (null, null);
			}

			var projectPath = matches[0].Groups[1].Value;
			var projectName = projectPath.Replace("/", "__");

			return (projectPath, projectName);
		}

		public override bool CanDownload(string pluginKey)
		{
			if(pluginKey.Contains(".git")) return false;
			(string projectPath, string projectName) = GetProjectInfoFromPath(pluginKey);
			if(string.IsNullOrEmpty(projectPath) || string.IsNullOrEmpty(projectName)) return false;
			return true;
		}

		public override string GetShortTitle(string pluginKey)
		{
			return pluginKey;
		}

		public async override Task UpdatePlugin(
			UserConfig.Info pluginInfo,
			SystemConfig.Info systemInfo,
			string addonFolder,
			PluginStatusNotifier notifier,
			CancellationToken token = default(CancellationToken)
		)
		{
			using var client = clientFactory.CreateClient();
			client.DefaultRequestHeaders.UserAgent.ParseAdd("WoW Plugin Manager");

			notifier.StatusChanged(Status.CheckingForUpdate, "Checking For Update");

			(string projectPath, string projectName) = GetProjectInfoFromPath(pluginInfo.Path);
			var requestedRef = pluginInfo?.VersionRef;

			var releaseInfoUrl = string.IsNullOrWhiteSpace(requestedRef)
				? $"https://api.github.com/repos/{projectPath}/releases/latest"
				: $"https://api.github.com/repos/{projectPath}/releases/tags/{requestedRef}";
			var releaseInfoResponse = await client.GetAsync(releaseInfoUrl);
			if(!releaseInfoResponse.IsSuccessStatusCode)
			{
				notifier.StatusChanged(Status.Failure, "Failed to get releases information from github");
				return;
			}

			var releaseInfo = JsonConvert.DeserializeObject<GitHubApiRelease>(await releaseInfoResponse.Content.ReadAsStringAsync());
			var remoteHash = releaseInfo.Name;

			var allFilesInstalled = AllInstalledFoldersExist(systemInfo, addonFolder);
			var alreadyUpToDate = systemInfo.Hash == remoteHash && allFilesInstalled;
			if(alreadyUpToDate)
			{
				notifier.StatusChanged(Status.AlreadyUpToDate, "Already Up-to-date");
				return;
			}

			string zipUrl;
			if(releaseInfo.Assets.Any())
			{
				// TODO: This is a pretty wild guess - if they have more than 1 zip this may grab the wrong one
				var targetAssetName = releaseInfo.Assets.Where(a => a.Name.Contains(".zip")).FirstOrDefault()?.Name;
				if(string.IsNullOrWhiteSpace(targetAssetName))
				{
					notifier.StatusChanged(Status.AlreadyUpToDate, "Failed to find a valid asset");
					return;
				}
				zipUrl = $"https://github.com/{projectPath}/releases/download/{releaseInfo.Name}/{targetAssetName}";
			}
			else
			{
				zipUrl = releaseInfo.Zipball_Url;
			}

			await zipUrlInstallerComponent.InstallFromZipUrl(zipUrl, systemInfo, addonFolder, notifier, remoteHash, client, token);
		}

		public class GitHubApiRelease
		{
			public string Url { get; set; }
			public string HtmlUrl { get; set; }
			public string Assets_Url { get; set; }
			public string Upload_Url { get; set; }
			public string Tarball_Url { get; set; }
			public string Zipball_Url { get; set; }
			public int Id { get; set; }
			public string NodeId { get; set; }
			public string TagName { get; set; }
			public string TargetCommitish { get; set; }
			public string Name { get; set; }
			public string Body { get; set; }
			public bool Draft { get; set; }
			public bool Prerelease { get; set; }
			public string CreatedAt { get; set; }
			public string PublishedAt { get; set; }
			public GitHubApiReleaseAuthor Author { get; set; }
			public GitHubApiReleaseAsset[] Assets { get; set; }
		}

		public class GitHubApiReleaseAuthor
		{
			public string Login { get; set; }
			public int Id { get; set; }
			public string NodeId { get; set; }
			public string AvatarUrl { get; set; }
			public string GravatarId { get; set; }
			public string Url { get; set; }
			public string HtmlUrl { get; set; }
			public string FollowersUrl { get; set; }
			public string FollowingUrl { get; set; }
			public string GistsUrl { get; set; }
			public string StarredUrl { get; set; }
			public string SubscriptionsUrl { get; set; }
			public string OrganizationsUrl { get; set; }
			public string ReposUrl { get; set; }
			public string EventsUrl { get; set; }
			public string ReceivedEventsUrl { get; set; }
			public string Type { get; set; }
			public bool SiteAdmin { get; set; }
		}

		public class GitHubApiReleaseAsset
		{
			public string Url { get; set; }
			public string Browser_download_url { get; set; }
			public int Id { get; set; }
			public string Node_id { get; set; }
			public string Name { get; set; }
			public string Label { get; set; }
			public string State { get; set; }
			public string Content_type { get; set; }
			public int Size { get; set; }
			public int Download_count { get; set; }
			public string Created_at { get; set; }
			public string Updated_at { get; set; }
			public GitHubApiReleaseAssetUploader Uploader { get; set; }

			public class GitHubApiReleaseAssetUploader
			{
				public string Login { get; set; }
				public int Id { get; set; }
				public string Node_id { get; set; }
				public string Avatar_url { get; set; }
				public string Gravatar_id { get; set; }
				public string Url { get; set; }
				public string Html_url { get; set; }
				public string Followers_url { get; set; }
				public string Following_url { get; set; }
				public string Gists_url { get; set; }
				public string Starred_url { get; set; }
				public string Subscriptions_url { get; set; }
				public string Organizations_url { get; set; }
				public string Repos_url { get; set; }
				public string Events_url { get; set; }
				public string Received_events_url { get; set; }
				public string Type { get; set; }
				public bool SiteAdmin { get; set; }
			}
		}
	}
}
