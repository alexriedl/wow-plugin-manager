using Core.Extensions;
using Core.Notifiers;
using NLog;
using Newtonsoft.Json;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Threading;

namespace Core.Components.Installers
{
	public class CurseInstaller : PluginInstaller
	{
		private static readonly Logger Logger = LogManager.GetCurrentClassLogger();
		private readonly IHttpClientFactory clientFactory;
		private readonly ZipUrlInstallerComponent zipUrlInstallerComponent;

		public CurseInstaller(IHttpClientFactory clientFactory, ZipUrlInstallerComponent zipUrlInstallerComponent)
		{
			this.clientFactory = clientFactory;
			this.zipUrlInstallerComponent = zipUrlInstallerComponent;
		}

		private string GetIdFromPath(string path)
		{
			if(string.IsNullOrWhiteSpace(path)) return null;
			var search = path.ToLower();
			var index = search.IndexOf('?');
			if(index <= 0) return null;
			var id = search.Substring(index + 1);
			if(string.IsNullOrWhiteSpace(id)) return null;
			return id;
		}

		public override bool CanDownload(string pluginKey)
		{
			var isCurseAddon = pluginKey.Contains("www.curseforge.com");
			if(!isCurseAddon) return false;

			if(!pluginKey.Contains('?') || string.IsNullOrWhiteSpace(GetIdFromPath(pluginKey)))
			{
				Logger.Warn($"Invalid curse addon format for {pluginKey}");
				return false;
			}

			return isCurseAddon;
		}

		public override string GetShortTitle(string pluginKey)
		{
			var prefix = "wow/addons";
			var prefixIndex = pluginKey.IndexOf(prefix) + prefix.Length + 1;
			var suffixIndex = pluginKey.IndexOf('?');
			var title = pluginKey.Substring(prefixIndex, suffixIndex - prefixIndex);
			var result = title.ConvertSnakeCaseToTitleCase();
			return result;
		}

		public async override Task UpdatePlugin(
			UserConfig.Info pluginInfo,
			SystemConfig.Info systemInfo,
			string addonFolder,
			PluginStatusNotifier notifier,
			CancellationToken token = default(CancellationToken)
		)
		{
			var wowVersionSlug = "wow_classic";
			var releaseVersion = 1;
			using var client = clientFactory.CreateClient();

			notifier.StatusChanged(Status.CheckingForUpdate, "Checking For Update");

			var id = GetIdFromPath(pluginInfo.Path);
			if(string.IsNullOrWhiteSpace(id))
			{
				notifier.StatusChanged(Status.Failure, "Failed to parse plugin id. Ensure the curse plugin has the correct format");
				return;
			}

			var response = await client.GetAsync($"https://addons-ecs.forgesvc.net/api/v2/addon/{id}", token);
			if(!response.IsSuccessStatusCode)
			{
				notifier.StatusChanged(Status.Failure, "Failed to get update information from CurseForge");
				return;
			}

			var releaseInfo = JsonConvert.DeserializeObject<CurseAddonInfo>(await response.Content.ReadAsStringAsync());

			var latestRelease = releaseInfo?.latestFiles?.Length > 1
				? releaseInfo?.latestFiles?.FirstOrDefault(f => f.gameVersionFlavor == wowVersionSlug && f.releaseType == releaseVersion)
				: releaseInfo?.latestFiles?.FirstOrDefault();
			var remoteHash = latestRelease?.id;

			if(string.IsNullOrWhiteSpace(remoteHash))
			{
				notifier.StatusChanged(Status.Failure, $"Failed to find latest version for wow version {wowVersionSlug}");
				return;
			}

			var allFilesInstalled = AllInstalledFoldersExist(systemInfo, addonFolder);
			var alreadyUpToDate = systemInfo.Hash == remoteHash && allFilesInstalled;
			if(alreadyUpToDate)
			{
				notifier.StatusChanged(Status.AlreadyUpToDate, "Already Up-to-date");
				return;
			}

			var zipUrl = latestRelease.downloadUrl;
			await zipUrlInstallerComponent.InstallFromZipUrl(zipUrl, systemInfo, addonFolder, notifier, remoteHash, client, token);
		}

		// NOTE: https://twitchappapi.docs.apiary.io/

		public class CurseAddonInfo
		{
			public int id { get; set; }
			public string name { get; set; }
			public object authors { get; set; }
			public object attachments { get; set; }
			public string websiteUrl { get; set; }
			public int gameId { get; set; }
			public string summary { get; set; }
			public string defaultFileId { get; set; }
			public string downloadCount { get; set; }
			public CurseAddonFile[] latestFiles { get; set; }
			public object[] categories { get; set; }
			public int status { get; set; }
			public int primaryCategoryId { get; set; }
			public object categorySection { get; set; }
			public string slug { get; set; }
			public object[] gameVersionLatestFiles { get; set; }
			public bool isFeatured { get; set; }
			public double popularityScore { get; set; }
			public int gamePopularityRank { get; set; }
			public string primaryLanguage { get; set; }
			public string gameSlug { get; set; }
			public string gameName { get; set; }
			public string portalName { get; set; }
			public string dateModified { get; set; }
			public string dateCreated { get; set; }
			public string dateReleased { get; set; }
			public bool isAvailable { get; set; }
			public bool isExperiemental { get; set; }
		}

		public class CurseAddonFile
		{
			public string id { get; set; }
			public string displayName { get; set; }
			public string fileName { get; set; }
			public string fileDate { get; set; }
			public int fileLength { get; set; }
			public int releaseType { get; set; }
			public int fileStatus { get; set; }
			public string downloadUrl { get; set; }
			public bool isAlternate { get; set; }
			public int alternateFileId { get; set; }
			public object[] dependencies { get; set; }
			public bool isAvailable { get; set; }
			public object[] modules { get; set; }
			public string packageFingerprint { get; set; }
			public string[] gameVersion { get; set; }
			public object[] sortableGameVersion { get; set; }
			public object installMetadata { get; set; }
			public object changelog { get; set; }
			public bool hasInstallScript { get; set; }
			public bool isCompatibleWithClient { get; set; }
			public int categorySectionPackageType { get; set; }
			public int restrictProjectFileAccess { get; set; }
			public int projectStatus { get; set; }
			public int renderCacheId { get; set; }
			public object fileLegacyMappingId { get; set; }
			public int projectId { get; set; }
			public object parentProjectFileId { get; set; }
			public object parentFileLegacyMappingId { get; set; }
			public object fileTypeId { get; set; }
			public object exposeAsAlternative { get; set; }
			public string packageFingerprintId { get; set; }
			public string gameVersionDateReleased { get; set; }
			public int gameVersionMappingId { get; set; }
			public int gameVersionId { get; set; }
			public int gameId { get; set; }
			public bool isServerPack { get; set; }
			public object serverPackFileId { get; set; }
			public string gameVersionFlavor { get; set; }
		}
	}
}
