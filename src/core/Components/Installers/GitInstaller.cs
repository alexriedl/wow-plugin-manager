using Core.Extensions;
using Core.Notifiers;
using LibGit2Sharp;
using NLog;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Threading;
using System;

namespace Core.Components.Installers
{
	public class GitInstaller : PluginInstaller
	{
		private static readonly Logger Logger = LogManager.GetCurrentClassLogger();
		private readonly UninstallPluginComponent uninstallPluginComponent;
		private readonly InstallPluginComponent installPluginComponent;

		public GitInstaller(
			UninstallPluginComponent uninstallPluginComponent,
			InstallPluginComponent installPluginComponent)
		{
			this.uninstallPluginComponent = uninstallPluginComponent;
			this.installPluginComponent = installPluginComponent;
		}

		public override bool CanDownload(string pluginKey)
		{
			return pluginKey.Contains(".git");
		}

		public override string GetShortTitle(string pluginKey)
		{
			var regex = new Regex(@"((git|ssh|http(s)?)|(git@[\w\.]+))(:(//)?)([\w\.@\:/\-~]+)(\.git)(/)?", RegexOptions.Compiled);
			var matches = regex.Matches(pluginKey);
			if(matches.Count <= 0) return pluginKey;

			var trimmedUrl = matches[0].Groups[7].Value;
			var title = trimmedUrl.Substring(trimmedUrl.LastIndexOf('/') + 1);
			var result = title.ConvertSnakeCaseToTitleCase();
			return result;
		}

		public async override Task UpdatePlugin(
			UserConfig.Info pluginInfo,
			SystemConfig.Info systemInfo,
			string addonFolder,
			PluginStatusNotifier notifier,
			CancellationToken token = default(CancellationToken)
		)
		{
			await Task.Run(() => UpdatePluginSync(pluginInfo, systemInfo, addonFolder, notifier));
		}

		public void UpdatePluginSync(
			UserConfig.Info pluginInfo,
			SystemConfig.Info systemInfo,
			string addonFolder,
			PluginStatusNotifier notifier
		)
		{
			var gitUrl = pluginInfo.Path;
			var shortName = Path.GetFileNameWithoutExtension((new FileInfo(gitUrl)).Name).ToLower().Replace('_', '-');
			var repoPath = Path.Combine(OsInfo.GetDefaultTemporaryFilesDirectory(), shortName);

			notifier.StatusChanged(Status.CheckingForUpdate, "Cloning Project Repository");

			var repoExists = EnsureProjectIsCloned(repoPath, gitUrl);
			if(!repoExists)
			{
				notifier.StatusChanged(Status.Failure, "Failed to clone project repository");
				return;
			}

			using(var repo = new Repository(repoPath))
			{
				// NOTE: Ensure we have latest
				var remote = repo.Network.Remotes["origin"];
				var refspecs = remote.FetchRefSpecs.Select(x => x.Specification);
				Commands.Fetch(repo, remote.Name, refspecs, null, null);

				// NOTE: Ensure branch or tag exists, and check it out
				var requestedRef = pluginInfo?.VersionRef ?? "master";
				var remoteBranchName = $"origin/{requestedRef}";
				var branchExists = repo.Branches.Any((b) => b.FriendlyName.Equals(remoteBranchName));
				var tagExists = repo.Tags.Any((t) => t.FriendlyName.Equals(requestedRef));

				var localHash = systemInfo.Hash;
				var remoteHash = localHash;
				if(branchExists)
				{
					Commands.Checkout(repo, remoteBranchName);
					remoteHash = repo.Branches[remoteBranchName].Commits.First().Sha;
				}
				else if(tagExists)
				{
					Commands.Checkout(repo, requestedRef);
					remoteHash = repo.Tags[requestedRef].Target.Sha;
				}
				else
				{
					notifier.StatusChanged(Status.Failure, $"Requested ref {requestedRef} does not exist in {gitUrl}");
					return;
				}

				var allFilesInstalled = AllInstalledFoldersExist(systemInfo, addonFolder);
				var alreadyUpToDate = localHash == remoteHash && allFilesInstalled;
				if(alreadyUpToDate)
				{
					notifier.StatusChanged(Status.AlreadyUpToDate, "Already Up-to-date");
					return;
				}

				// TODO: This section could be pulled into a componened - exact same code as the ZipUrlInstallerComponent
				{
					notifier.StatusChanged(Status.RemovingOldFolders, "Removing Old Folders");
					var uninstallResult = uninstallPluginComponent.UninstallPlugin(addonFolder, systemInfo);
					uninstallResult.Switch(
						success => {
							systemInfo.Hash = null;
							systemInfo.LocalFolders = Array.Empty<string>();
						},
						failure => {
							// TODO: We failed to remove old files. It may not be safe to try to install the new files
							// aaaand there is no easy way to exit early from the main function while in a lambda...
							// maybe use the stupid trypick methods
						});

					notifier.StatusChanged(Status.Updating, "Updating");

					var installResults = installPluginComponent.InstallPlugin(addonFolder, repoPath);
					installResults.Switch(
						success => {
							// TODO: This is kind of gross. Easily forgotten for new installers
							notifier.StatusChanged(Status.Updated, "Updated");
							systemInfo.Hash = remoteHash;
							systemInfo.LocalFolders = success.FoldersInstalled;
						},
						invalidFolderStructure => notifier.StatusChanged(Status.Failure, "Plugin has an invalid folder structure"),
						nothingToInstall => notifier.StatusChanged(Status.Failure, "Plugin has no plugins to install")
					);
				}
			}
		}

		private bool EnsureProjectIsCloned(string repoPath, string gitUrl)
		{
			if(!Directory.Exists(Path.Combine(repoPath, ".git")))
			{
				try
				{
					Logger.Info($"Cloning {gitUrl}.");
					Repository.Clone(gitUrl, repoPath);
				}
				catch(Exception e)
				{
					Logger.Warn($"Failed to clone {gitUrl}");
					Logger.Info(e);
					return false;
				}
			}

			return true;
		}
	}
}
