using Core.Extensions;
using Core.Notifiers;
using NLog;
using System.IO;
using System.Net.Http;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Threading;

namespace Core.Components.Installers
{
	public class WowInterfaceInstaller : PluginInstaller
	{
		private static readonly Logger Logger = LogManager.GetCurrentClassLogger();
		private readonly IHttpClientFactory clientFactory;
		private readonly ZipUrlInstallerComponent zipUrlInstallerComponent;

		public WowInterfaceInstaller(IHttpClientFactory clientFactory, ZipUrlInstallerComponent zipUrlInstallerComponent)
		{
			this.clientFactory = clientFactory;
			this.zipUrlInstallerComponent = zipUrlInstallerComponent;
		}

		public override bool CanDownload(string pluginKey)
		{
			return pluginKey.Contains("wowinterface.com");
		}

		public override string GetShortTitle(string pluginKey)
		{
			var breakIndex = pluginKey.IndexOf('-');
			var title = pluginKey.Substring(breakIndex + 1);
			var result = title.ConvertSnakeCaseToTitleCase();
			return result;
		}

		public async override Task UpdatePlugin(
			UserConfig.Info pluginInfo,
			SystemConfig.Info systemInfo,
			string addonFolder,
			PluginStatusNotifier notifier,
			CancellationToken token = default(CancellationToken)
		)
		{
			using var client = clientFactory.CreateClient();

			notifier.StatusChanged(Status.CheckingForUpdate, "Checking For Update");

			var zipUrl = await GetDownloadUrl(pluginInfo, client);

			var remoteHash = zipUrl;
			var allFilesInstalled = AllInstalledFoldersExist(systemInfo, addonFolder);
			var alreadyUpToDate = systemInfo.Hash == remoteHash && allFilesInstalled;
			if(alreadyUpToDate)
			{
				notifier.StatusChanged(Status.AlreadyUpToDate, "Already Up-to-date");
				return;
			}

			await zipUrlInstallerComponent.InstallFromZipUrl(zipUrl, systemInfo, addonFolder, notifier, remoteHash, client, token);
		}

		private async Task<string> GetDownloadUrl(UserConfig.Info pluginInfo, HttpClient client)
		{
			var parentPageResponse = await client.GetAsync(pluginInfo.Path);
			if(!parentPageResponse.IsSuccessStatusCode)
			{
				Logger.Warn("Failed to download wow-interface download page");
				return null;
			}

			var downloadLine = "";
			using(var stream = await parentPageResponse.Content.ReadAsStreamAsync())
			{
				using(var reader = new StreamReader(stream))
				{
					var line = "";
					while((line = await reader.ReadLineAsync()) != null)
					{
						if(line?.Contains("https://cdn.wowinterface.com/downloads") == true)
						{
							break;
						}
					}
					downloadLine = line;
				}
			}

			if(string.IsNullOrWhiteSpace(downloadLine))
			{
				Logger.Warn("Failed to find zip download line in wow-interface download page");
				return null;
			}

			var regex = new Regex(@"href=""(.*)\?.*", RegexOptions.Compiled);
			var matches = regex.Matches(downloadLine);

			if(matches.Count <= 0)
			{
				Logger.Warn("Failed to find zip download link in wow-interface download page");
				return null;
			}
			var zipUrl = matches[0].Groups[1].Value;
			return zipUrl;
		}
	}
}
