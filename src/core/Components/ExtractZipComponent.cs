using NLog;
using OneOf;
using System.IO.Compression;
using System.IO;
using System.Threading.Tasks;
using System.Threading;

namespace Core.Components
{
	public abstract class ExtractZipResult : OneOfBase<ExtractZipResult.Success, ExtractZipResult.Failure>
	{
		public class Success : ExtractZipResult { public string ExtractDestination { get; set; } }
		public class Failure : ExtractZipResult { public string Reason { get; set; } }
	}

	public class ExtractZipComponent
	{
		private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

		public string GetPluginNameFromZip(string zipFile)
		{
			using(var archive = ZipFile.OpenRead(zipFile))
			{
				foreach(var entry in archive.Entries)
				{
					if(entry.Name.EndsWith(".toc"))
					{
						return Path.GetFileNameWithoutExtension(entry.Name);
					}
				}
			}

			return Path.GetFileNameWithoutExtension(zipFile);
		}

		public async Task<ExtractZipResult> ExtractZip(string zipFile, string destinationFolder, CancellationToken token = default(CancellationToken))
		{
			var pluginName = GetPluginNameFromZip(zipFile);
			var extractPath = Path.Combine(destinationFolder, pluginName);

			if(Directory.Exists(extractPath))
			{
				Directory.Delete(extractPath, recursive: true);
			}

			await Task.Run(() => ZipFile.ExtractToDirectory(zipFile, extractPath, overwriteFiles: true));

			return new ExtractZipResult.Success
			{
				ExtractDestination = extractPath
			};
		}

		public async Task<ExtractZipResult> ExtractZip(byte[] content, string destinationFolder, CancellationToken token = default(CancellationToken))
		{
			var guid = System.Guid.NewGuid().ToString().Trim();
			var destinationZip = Path.Combine(destinationFolder, $"{guid}.zip");

			if(File.Exists(destinationZip))
			{
				File.Delete(destinationZip);
			}

			await File.WriteAllBytesAsync(destinationZip, content, token);

			var f = new FileInfo(destinationZip);
			if(!f.Exists)
			{
				Logger.Warn("Failed to write zip contents to file");
				return new ExtractZipResult.Failure
				{
					Reason = "Failed to write zip contents to file"
				};
			}

			var result = await ExtractZip(destinationZip, destinationFolder, token);

			File.Delete(destinationZip);

			return result;
		}
	}
}
