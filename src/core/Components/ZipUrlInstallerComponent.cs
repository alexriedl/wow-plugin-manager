using Core.Notifiers;
using NLog;
using System.Net.Http;
using System.Threading.Tasks;
using System.Threading;
using System;

namespace Core.Components
{
	public class ZipUrlInstallerComponent
	{
		private static readonly Logger Logger = LogManager.GetCurrentClassLogger();
		private readonly IHttpClientFactory clientFactory;
		private readonly UninstallPluginComponent uninstallPluginComponent;
		private readonly ExtractZipComponent extractZipComponent;
		private readonly InstallPluginComponent installPluginComponent;

		public ZipUrlInstallerComponent(IHttpClientFactory clientFactory,
			UninstallPluginComponent uninstallPluginComponent,
			ExtractZipComponent extractZipComponent,
			InstallPluginComponent installPluginComponent)
		{
			this.clientFactory = clientFactory;
			this.uninstallPluginComponent = uninstallPluginComponent;
			this.extractZipComponent = extractZipComponent;
			this.installPluginComponent = installPluginComponent;
		}

		public Task InstallFromZipUrl(
			string zipUrl,
			SystemConfig.Info systemInfo,
			string addonFolder,
			PluginStatusNotifier notifier,
			string newVersionHash,
			CancellationToken token = default(CancellationToken)
		)
		{
			using var client = clientFactory.CreateClient();
			return InstallFromZipUrl(zipUrl, systemInfo, addonFolder, notifier, newVersionHash, client, token);
		}

		public async Task InstallFromZipUrl(
			string zipUrl,
			SystemConfig.Info systemInfo,
			string addonFolder,
			PluginStatusNotifier notifier,
			string newVersionHash,
			HttpClient client,
			CancellationToken token = default(CancellationToken)
		)
		{
			if(string.IsNullOrWhiteSpace(zipUrl))
			{
				notifier.StatusChanged(Status.Failure, "Failed to find zip download url");
				return;
			}

			notifier.StatusChanged(Status.RemovingOldFolders, "Removing Old Folders");
			var uninstallResult = uninstallPluginComponent.UninstallPlugin(addonFolder, systemInfo);
			uninstallResult.Switch(
				success => {
					systemInfo.Hash = null;
					systemInfo.LocalFolders = Array.Empty<string>();
				},
				failure => {
					// TODO: We failed to remove old files. It may not be safe to try to install the new files
					// aaaand there is no easy way to exit early from the main function while in a lambda...
					// maybe use the stupid trypick methods
				});

			notifier.StatusChanged(Status.Updating, "Updating");

			var zipResponse = await client.GetAsync(zipUrl, token);
			if(!zipResponse.IsSuccessStatusCode)
			{
				notifier.StatusChanged(Status.Failure, "Failed to download plugin zip");
				return;
			}

			var extractResults = await extractZipComponent.ExtractZip(
				await zipResponse.Content.ReadAsByteArrayAsync(),
				OsInfo.GetDefaultTemporaryFilesDirectory(),
				token);
			if(!extractResults.TryPickT0(out var success, out var failure))
			{
				notifier.StatusChanged(Status.Failure, failure.Reason);
				return;
			}

			var installResults = installPluginComponent.InstallPlugin(addonFolder, success.ExtractDestination);
			installResults.Switch(
				success => {
					// TODO: This is kind of gross. Easily forgotten for new installers
					notifier.StatusChanged(Status.Updated, "Updated");
					systemInfo.Hash = newVersionHash;
					systemInfo.LocalFolders = success.FoldersInstalled;
				},
				invalidFolderStructure => notifier.StatusChanged(Status.Failure, "Plugin has an invalid folder structure"),
				nothingToInstall => notifier.StatusChanged(Status.Failure, "Plugin has no plugins to install")
			);
		}
	}
}
