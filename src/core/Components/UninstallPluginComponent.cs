using NLog;
using OneOf;
using System.IO;
using System.Linq;
using System;

namespace Core.Components
{
	public abstract class UninstallPluginResult : OneOfBase<UninstallPluginResult.Success, UninstallPluginResult.Failure>
	{
		public class Success : UninstallPluginResult {}
		public class Failure : UninstallPluginResult { public string Reason { get; set; } public Exception Exception { get; set; } }
	}

	public class UninstallPluginComponent
	{
		private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

		public UninstallPluginResult UninstallPlugin(string interfaceFolder, SystemConfig.Info pluginInfo)
		{
			var foldersToRemove = pluginInfo?.LocalFolders;
			if(foldersToRemove == null || !foldersToRemove.Any())
			{
				return new UninstallPluginResult.Success();
			}

			Logger.Info($"Deleting plugin folders: [{string.Join(",", foldersToRemove)}]");
			try
			{
				foreach(var folderToRemove in foldersToRemove)
				{
					var path = Path.Combine(interfaceFolder, folderToRemove);
					if(Directory.Exists(path))
					{
						Directory.Delete(path, recursive: true);
					}
				}
			}
			catch(Exception e)
			{
				Logger.Error(e, $"Failed to delete folders");
				return new UninstallPluginResult.Failure
				{
					Reason = "System Failed to delete files.",
					Exception = e
				};
			}

			return new UninstallPluginResult.Success();
		}
	}
}
