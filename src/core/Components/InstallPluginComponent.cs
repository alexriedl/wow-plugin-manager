using Core.Extensions;
using NLog;
using OneOf;
using System.IO;
using System.Linq;

namespace Core.Components
{
	public abstract class InstallPluginResult : OneOfBase<
		InstallPluginResult.Success,
		InstallPluginResult.InvalidPluginStructure,
		InstallPluginResult.NothingToInstall>
	{
		public class Success : InstallPluginResult { public string[] FoldersInstalled { get; set; } }
		public class InvalidPluginStructure : InstallPluginResult {}
		public class NothingToInstall : InstallPluginResult {}
	}

	public class InstallPluginComponent
	{
		private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

		public InstallPluginResult InstallPlugin(string addonFolder, string folderToInstall)
		{
			return InstallPlugin(addonFolder, new DirectoryInfo(folderToInstall));
		}

		public InstallPluginResult InstallPlugin(string addonFolder, DirectoryInfo folderToInstall)
		{
			if(!folderToInstall.Exists)
			{
				return new InstallPluginResult.InvalidPluginStructure();
			}

			var tocFiles = folderToInstall.GetFiles("*.toc");
			if(tocFiles.Length > 1)
			{
				return new InstallPluginResult.InvalidPluginStructure();
			}

			if(tocFiles.Length == 1)
			{
				var folderToInstallName = Path.GetFileNameWithoutExtension(tocFiles[0].Name);
				Logger.Info($"Installing plugin folder: {folderToInstallName}");
				var target = Path.Combine(addonFolder, folderToInstallName);

				if(Directory.Exists(target))
				{
					Directory.Delete(target, recursive: true);
				}

				folderToInstall.Copy(target, recursive: true, overwrite: false);
				return new InstallPluginResult.Success
				{
					FoldersInstalled = new string[] { folderToInstallName }
				};
			}

			var subdirectories = folderToInstall.GetDirectories();
			if(subdirectories.Length > 0)
			{
				var results = subdirectories.Select(d => InstallPlugin(addonFolder, d)).ToArray();
				if(results.Any(r => r is InstallPluginResult.InvalidPluginStructure)) return new InstallPluginResult.InvalidPluginStructure();
				var installedFolders = results
					.Where(r => r is InstallPluginResult.Success)
					.Select(r => r as InstallPluginResult.Success)
					.SelectMany(r => r.FoldersInstalled)
					.ToArray();
				return new InstallPluginResult.Success
				{
					FoldersInstalled = installedFolders
				};
			}

			return new InstallPluginResult.NothingToInstall();
		}
	}
}
