using NLog;

namespace Core.Notifiers
{
	public class LogPluginStatusNotifier : PluginStatusNotifier
	{
		private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

		public LogPluginStatusNotifier(string pluginName) : base(pluginName)
		{
		}

		public override void StatusChanged(Status newStatus, string message)
		{
			System.Console.WriteLine($"{PluginName} -> {StatusToString(newStatus)} - {message}");
		}

		private string StatusToString(Status status)
		{
			switch(status)
			{
				case Status.AlreadyUpToDate: return "Already Up-to-date";
				case Status.CheckingForUpdate: return "Checking For Update";
				case Status.Failure: return "Failure";
				case Status.Removed: return "Removed";
				case Status.RemovingOldFolders: return "Removing Old Folders";
				case Status.Updated: return "Updated";
				case Status.Updating: return "Updating";
			}

			return "UNKNOWN";
		}
	}
}
