using System.Collections.Generic;
using System.Linq;
using System;

namespace Core.Notifiers
{
	public class PluginStatusNotifierFactory
	{
		private List<Type> Notifiers { get; set; } = new List<Type>();

		public PluginStatusNotifierFactory()
		{
		}

		public void RegisterNotifier<T>() where T : PluginStatusNotifier
		{
			Notifiers.Add(typeof(T));
		}

		public void RegisterNotifier(Type T)
		{
		}

		public PluginStatusNotifier Create(string pluginName)
		{
			var container = new MultiplePluginStatusNotifier(pluginName);

			foreach(var notifier in Notifiers)
			{
				var newObject = Activator.CreateInstance(notifier, pluginName);
				var newNotifier = newObject as PluginStatusNotifier;
				if(newNotifier != null) container.AddNotifier(newNotifier);
			}

			return container;
		}

		private class MultiplePluginStatusNotifier : PluginStatusNotifier
		{
			private List<PluginStatusNotifier> Notifiers { get; set; } = new List<PluginStatusNotifier>();

			public MultiplePluginStatusNotifier(string pluginName) : base(pluginName)
			{
			}

			public void AddNotifier(PluginStatusNotifier newNotifier)
			{
				Notifiers.Add(newNotifier);
			}

			public override void StatusChanged(Status newStatus, string message)
			{
				if(Notifiers == null || !Notifiers.Any()) return;

				foreach(var notifier in Notifiers)
				{
					notifier.StatusChanged(newStatus, message);
				}
			}
		}
	}
}
