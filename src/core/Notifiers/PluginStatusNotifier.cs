namespace Core.Notifiers
{
	public abstract class PluginStatusNotifier
	{
		protected string PluginName { get; private set; }

		public PluginStatusNotifier(string pluginName)
		{
			PluginName = pluginName;
		}

		public abstract void StatusChanged(Status newStatus, string message);
	}

	public enum Status
	{
		Idle,
		Failure,
		Removed,
		CheckingForUpdate,
		RemovingOldFolders,
		Updating,
		Updated,
		AlreadyUpToDate,
	}
}
