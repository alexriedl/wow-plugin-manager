using System.Collections.Concurrent;
using System.Linq;
using System.Text;
using System;

namespace Core.Notifiers
{
	public class ConsolePluginStatusNotifier : PluginStatusNotifier
	{
		private static ConcurrentDictionary<string, StatusInfo> PluginStatus { get; set; } = new ConcurrentDictionary<string, StatusInfo>();
		private static readonly object Lock = new object();

		public ConsolePluginStatusNotifier(string pluginName) : base(pluginName)
		{
			PluginStatus.TryAdd(pluginName, new StatusInfo
			{
				Status = Status.Idle,
				Message = "Idle"
			});
		}

		private static void Render()
		{
			var keys = PluginStatus.Keys.OrderBy(p => p).ToArray();
			var values = PluginStatus.Values.ToArray();

			var longestKey = keys
				.OrderByDescending(k => k.Length)
				.FirstOrDefault()?.Length ?? 1;

			var longestStatus = values
				.Select(v => StatusToString(v.Status))
				.OrderByDescending(s => s.Length)
				.FirstOrDefault()?.Length ?? 1;

			var longestMessage = values
				.Select(v => v.Message)
				.OrderByDescending(s => s.Length)
				.FirstOrDefault()?.Length ?? 1;

			var header = $"| {"Plugin".PadRight(longestKey, ' ')} | {"Status".PadRight(longestStatus, ' ')} | {"Message".PadRight(longestMessage, ' ')} |";
			var line = $"+-{"".PadRight(longestKey, '-')}-+-{"".PadRight(longestStatus, '-')}-+-{"".PadRight(longestMessage, '-')}-+";

			lock(Lock)
			{
				Console.Clear();

				Console.WriteLine(line);
				Console.WriteLine(header);
				Console.WriteLine(line);

				foreach(var key in keys)
				{
					PluginStatus.TryGetValue(key, out var status);
					Console.WriteLine($"| {key.PadRight(longestKey, ' ')} | {StatusToString(status.Status).PadRight(longestStatus, ' ')} | {status.Message.PadRight(longestMessage, ' ')} |");
				}

				Console.WriteLine(line);
			}
		}

		public override void StatusChanged(Status newStatus, string message)
		{
			if(PluginStatus.ContainsKey(PluginName))
			{
				PluginStatus.TryGetValue(PluginName, out var oldStatus);
				var status = new StatusInfo
				{
					Status = newStatus,
					Message = message
				};
				PluginStatus.TryUpdate(PluginName, status, oldStatus);
			}
			else
			{
				// ???
			}

			Render();
		}

		private static string StatusToString(Status status)
		{
			switch(status)
			{
				case Status.Idle: return "Idle";
				case Status.AlreadyUpToDate: return "Already Up-to-date";
				case Status.CheckingForUpdate: return "Checking For Update";
				case Status.Failure: return "Failure";
				case Status.Removed: return "Removed";
				case Status.RemovingOldFolders: return "Removing Old Folders";
				case Status.Updated: return "Updated";
				case Status.Updating: return "Updating";
			}

			return "UNKNOWN";
		}

		private class StatusInfo
		{
			public Status Status { get; set; }
			public string Message { get; set; }
		}
	}
}
