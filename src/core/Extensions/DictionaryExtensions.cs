using System.Collections.Generic;

namespace Core.Extensions
{
	public static class DictionaryExtensions
	{
		public static V Find<K, V>(this IDictionary<K, V> source, K key)
		{
			if(source == null || !source.ContainsKey(key)) return default(V);
			return source[key];
		}

		public static V FindOrNew<K, V>(this IDictionary<K, V> source, K key) where V : new()
		{
			if(source == null) throw new System.ArgumentNullException();
			if(!source.ContainsKey(key)) source.Add(key, new V());
			return source[key];
		}
	}
}
