using System.Linq;
using System;

namespace Core.Extensions
{
	public static class StringExtensions
	{
		public static int GetDeterministicHashCode(this string str)
		{
			unchecked
			{
				int hash1 = (5381 << 16) + 5381;
				int hash2 = hash1;

				for (int i = 0; i < str.Length; i += 2)
				{
					hash1 = ((hash1 << 5) + hash1) ^ str[i];
					if (i == str.Length - 1)
						break;
					hash2 = ((hash2 << 5) + hash2) ^ str[i + 1];
				}

				return hash1 + (hash2 * 1566083941);
			}
		}

		public static string ConvertSnakeCaseToTitleCase(this string value)
		{
			return value
				.Split(new [] {"_", "-"}, StringSplitOptions.RemoveEmptyEntries)
				.Select(s => char.ToUpperInvariant(s[0]) + s.Substring(1))
				.Aggregate(string.Empty, (s1, s2) => s1 + s2);
		}

		public static string ConvertTitleCaseToPascelCase(this string value)
		{
			return char.ToLowerInvariant(value[0]) + value.Substring(1);
		}

		public static string ConvertSnakeCaseToPascelCase(this string value)
		{
			return value.ConvertSnakeCaseToTitleCase().ConvertTitleCaseToPascelCase();
		}

		public static string ConvertTitleCaseToSnakeCase(this string value)
		{
			return string.Concat(value.Select((x, i) => i > 0 && char.IsUpper(x) ? $"-{x}" : $"{x}")).ToLower();
		}

		public static string TrimSuffix(this string value, string suffix)
		{
			return value.Substring(0, value.LastIndexOf(suffix));
		}
	}
}
