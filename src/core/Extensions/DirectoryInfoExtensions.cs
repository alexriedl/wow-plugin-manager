using System.IO;

namespace Core.Extensions
{
	public static class DirectoryInfoExtensions
	{
		public static void Copy(this DirectoryInfo source, string destinationName, bool recursive = false, bool overwrite = false)
		{
			if(!source.Exists)
			{
				throw new DirectoryNotFoundException($"Source directory does not exist or could not be found: {source.FullName}");
			}

			if(!Directory.Exists(destinationName))
			{
				Directory.CreateDirectory(destinationName);
			}

			foreach(var file in source.GetFiles())
			{
				var targetFile = Path.Combine(destinationName, file.Name);
				file.CopyTo(targetFile, overwrite);
			}

			if(recursive)
			{
				foreach(var subdir in source.GetDirectories())
				{
					var targetDir = Path.Combine(destinationName, subdir.Name);
					subdir.Copy(targetDir, recursive, overwrite);
				}
			}
		}
	}
}
