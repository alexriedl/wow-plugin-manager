using NLog;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json.Serialization;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.IO;

namespace Core
{
	public static class Config
	{
		public static readonly string CURRENT_SYSTEM_CONFIG_SCHEMA_VERSION = "1.0";
		public static readonly string CURRENT_USER_CONFIG_SCHEMA_VERSION = "1.0";
		private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

		// TODO: We registered a version of this for injection. is there a way we can pull that in somehow?
		private static readonly JsonSerializerSettings JsonSettings = new JsonSerializerSettings
		{
			ContractResolver = new DefaultContractResolver
			{
				NamingStrategy = new SnakeCaseNamingStrategy(),
			},
			Formatting = Formatting.Indented,
		};
		private static readonly JsonSerializer JsonSerializer = new JsonSerializer()
		{
			ContractResolver = new DefaultContractResolver
			{
				NamingStrategy = new SnakeCaseNamingStrategy(),
			},
		};

		public static void EnsureConfigsExist()
		{
			if(!File.Exists(SystemConfig.FullFileName))
			{
				Logger.Info($"Failed to find System Config at '{SystemConfig.FullFileName}', creating a new one.");
				Config.Save(new SystemConfig());
			}
			if(!File.Exists(UserConfig.FullFileName))
			{
				Logger.Info($"Failed to find User Config at '{UserConfig.FullFileName}', creating a new one.");
				Config.Save(new UserConfig());
			}
		}

		public static void PrepareTemporaryFolder(SystemConfig systemConfig)
		{
			DirectoryInfo directory = new DirectoryInfo(OsInfo.GetDefaultTemporaryFilesDirectory());
			if(!directory.Exists) directory.Create();
		}

		public static SystemConfig LoadSystemConfig(string nonDefaultConfigPath = null)
		{
			var text = File.ReadAllText(string.IsNullOrWhiteSpace(nonDefaultConfigPath) ? SystemConfig.FullFileName : nonDefaultConfigPath);
			var raw = JToken.Parse(text);

			var schemaVersion = (string)raw["schema_version"];
			if(schemaVersion != "1.0")
			{
				Logger.Error("Unsupported system config version");
				throw new System.NotSupportedException("Unsupported system config version");
			}

			return new SystemConfig
			{
				SchemaVersion = schemaVersion,
				InterfaceFolder = (string)raw["interface_folder"],
				PluginInfo = raw["plugin_info"]?.ToObject<Dictionary<string, Core.SystemConfig.Info>>(JsonSerializer)
			};
		}

		public static UserConfig LoadUserConfig(string nonDefaultConfigPath = null)
		{
			var text = File.ReadAllText(string.IsNullOrWhiteSpace(nonDefaultConfigPath) ? UserConfig.FullFileName : nonDefaultConfigPath);
			var raw = JToken.Parse(text);

			var schemaVersion = (string)raw["schema_version"];
			if(schemaVersion != "1.0")
			{
				Logger.Error("Unsupported user config version");
				throw new System.NotSupportedException("Unsupported user config version");
			}

			var pluginInfosList = new List<UserConfig.Info>();
			var rawPluginInfos = raw["plugins"]?.ToObject<object[]>(JsonSerializer);
			foreach(object pluginInfo in rawPluginInfos)
			{
				if(pluginInfo is string)
				{
					pluginInfosList.Add(new UserConfig.Info
					{
						Path = pluginInfo as string,
						VersionRef = null
					});
				}
				else if(pluginInfo is JToken)
				{
					var token = pluginInfo as JToken;
					pluginInfosList.Add(token.ToObject<UserConfig.Info>(JsonSerializer));
				}
				else
				{
					var stringPlugin = JsonConvert.SerializeObject(pluginInfo);
					Logger.Warn($"Failed to parse plugin in user's config: {stringPlugin}");
				}
			}

			return new UserConfig
			{
				SchemaVersion = schemaVersion,
				Plugins = pluginInfosList.ToArray(),
			};
		}

		public static bool Save(SystemConfig config)
		{
			config.SchemaVersion = CURRENT_SYSTEM_CONFIG_SCHEMA_VERSION;
			File.WriteAllText(SystemConfig.FullFileName, JsonConvert.SerializeObject(config, JsonSettings));
			// TODO: Do something better here
			return true;
		}

		public static bool Save(UserConfig config)
		{
			var root = new JObject();
			root["schema_version"] = CURRENT_USER_CONFIG_SCHEMA_VERSION;

			var plugins = new JArray();
			foreach(var plugin in config.Plugins)
			{
				if(string.IsNullOrWhiteSpace(plugin.Path)) continue;

				if(string.IsNullOrWhiteSpace(plugin.VersionRef))
				{
					plugins.Add(plugin.Path);
				}
				else
				{
					plugins.Add(JToken.FromObject(new UserConfig.Info
					{
						Path = plugin.Path,
						VersionRef = plugin.VersionRef,
					}, JsonSerializer));
				}
			}
			root["plugins"] = plugins;

			File.WriteAllText(UserConfig.FullFileName, root.ToString());
			// TODO: Do something better here
			return true;
		}
	}

	public class SystemConfig
	{
		public static readonly string FileName = "system-config.json";
		public static readonly string FullFileName = Path.Combine(OsInfo.GetDefaultConfigFilesDirectory(), FileName);

		public string SchemaVersion { get; set; } = Config.CURRENT_SYSTEM_CONFIG_SCHEMA_VERSION;
		public string InterfaceFolder { get; set; }
		public Dictionary<string, Info> PluginInfo { get; set; } = new Dictionary<string, Info>();

		public class Info
		{
			public string Hash { get; set; }
			public string[] LocalFolders { get; set; } = System.Array.Empty<string>();

			[JsonIgnore]
			public string TempRef { get; set; }
		}
	}

	public class UserConfig
	{
		public static readonly string FileName = "user-config.json";
		public static readonly string FullFileName = Path.Combine(OsInfo.GetDefaultConfigFilesDirectory(), FileName);

		public string SchemaVersion { get; set; } = Config.CURRENT_USER_CONFIG_SCHEMA_VERSION;
		public Info[] Plugins { get; set; } = System.Array.Empty<Info>();

		public class Info
		{
			public string Path { get; set; }
			public string VersionRef { get; set; }
		}
	}
}
