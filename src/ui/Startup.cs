using Microsoft.Extensions.DependencyInjection;
using System;

namespace UI
{
	public static class Startup
	{
		public static IServiceProvider RegisterDependencies(IServiceCollection services = null)
		{
			if(services == null) services = new ServiceCollection();

			Core.Startup.RegisterDependencies(services);

			return services.BuildServiceProvider();
		}
	}
}
