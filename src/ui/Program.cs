﻿using Avalonia.Logging.Serilog;
using Avalonia.ReactiveUI;
using Avalonia;

namespace UI
{
	public class Program
	{
		public static void StartUIAndBlock(string[] args = null)
		{
			if(args == null) args = new string[] {};

			BuildAvaloniaApp()
				.StartWithClassicDesktopLifetime(args);
		}

		public static AppBuilder BuildAvaloniaApp()
		{
			return AppBuilder.Configure<App>()
				.UsePlatformDetect()
				.LogToDebug()
				.UseReactiveUI();
		}
	}
}
