using Core.UseCases;
using System.Collections.ObjectModel;
using static Core.UseCases.GetPluginListUseCase;

namespace UI.ViewModels
{
	public class PluginListViewModel : ViewModelBase
	{
		private GetPluginListUseCase GetPluginListUseCase { get; set; }

		public ObservableCollection<PluginStatus> Items { get; }

		public PluginListViewModel(GetPluginListUseCase getPluginListUseCase)
		{
			GetPluginListUseCase = getPluginListUseCase;

			Items = new ObservableCollection<PluginStatus>();
			UpdatePluginList();
		}

		public void UpdatePluginList()
		{
			var plugins = GetPluginListUseCase.GetPluginList();
			Items.Clear();
			foreach(var plugin in plugins)
			{
				Items.Add(plugin);
			}
		}
	}
}
