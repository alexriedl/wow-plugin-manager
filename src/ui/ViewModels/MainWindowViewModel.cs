﻿using Core.UseCases;
using Microsoft.Extensions.DependencyInjection;
using ReactiveUI;
using System.Reactive.Linq;
using System;

namespace UI.ViewModels
{
	public class MainWindowViewModel : ViewModelBase
	{
		public PluginListViewModel List { get; }
		public IServiceProvider ServiceProvider { get; set; }

		public MainWindowViewModel()
		{
			ServiceProvider = Startup.RegisterDependencies();

			var getPluginListUseCase = ServiceProvider.GetService<GetPluginListUseCase>();
			Content = List = new PluginListViewModel(getPluginListUseCase);
		}

		private ViewModelBase content;
		public ViewModelBase Content
		{
			get => content;
			private set => this.RaiseAndSetIfChanged(ref content, value);
		}
	}
}
